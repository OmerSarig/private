
# Private

Encryption tool for python files and packages.

# Features

* Encrypting python scripts, packages and directories so they can be used as drop-in replacements.
* Loading key from environment variable
* Validating decrypted module against SHA256 signature
* Pack encrypted packages into executable zip file
* Works both with python 2 and python 3

# Usage

### Encryption:

    $ private [--key <key>] [--output <directory>] [-z] [--predicate <expression>] file.py [<path>...]

### Execution:

    $ private --output out example-0.0-py2-none-any.whl
    $ pip install out/example-0.0-py2-none-any.whl
    $ export PRIVATE_KEY=$(cat out/key.txt)
    # Use installed package as usual

The key must be hexadecimal string of length 32.
The file type is recognized by extension, Currently only .py and .whl files are supported.

### Encrypting source distribution

It is possible to encrypt source distributions (.tar.gz files), but private will first
build a wheel and then convert it, so the final output is alway a wheel.

### Code removal

The is an option to "tag" the source code with decorators.
Then, It is possible to pass expression to the private script that specifies
which tags to remove completely from the source.

For example:

    # example.py
    import private

    @private.tag("a", "b")
    def foo():
        return "foo"

    @private.tag("b", "c")
    def bar():
        return "bar"

    @private.tag("z")
    def baz():
        return "baz"

Then, code parts to remove can be specified with regular python expression.
To remove only the "bar" function, the predicate is simply: "c".
To remove "foo" and "baz" the predicate is "a or z", or "not c".

The private script is called like this:

    $ private --predicate "b and not a or z" example.py

will create encrypted file that contains only the function "foo".

### Packing into zip archive

It is possible to pack multiple packages into one executable zip file.
For example:

    $ mkdir test
    $ cd test
    $ pip download poetry
    $ private -z --output out *
    $ cd out
    $ export PRIVATE_KEY=$(cat key.txt)
    $ python packed.zip poetry

The result zip file can be directly executed and can call every `console_script`
in the packed packages.
Currently, the -z flag can be used only if the given paths are wheels or source distributions.

WARNING: the load time can be *very* slow with many pacakges.

# Testing

The package doesn't have it's own test suit.
It is tested in python 2 with the compiledb package as a wheel, and in python 3
with the full test suit of the construct package, on the original repository.

# Limitations

Private is not fully tested and is a hobby project in alpha stage, Use it at your own risk.

# Future plans

* Add support for parallel execution
* encrypt with faster crypto libraries
* Full coverage and functionality testing
* Adding documentation
