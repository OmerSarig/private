OUTPUT_DIR=test_output

if [ ! -e construct ]; then
git clone --depth 1 https://github.com/construct/construct
fi
chmod -R a+r construct/

rm -rf $OUTPUT_DIR

private -o $OUTPUT_DIR construct
export PRIVATE_KEY=$(cat $OUTPUT_DIR/key.txt)

cd $OUTPUT_DIR/construct
python3.8 -m pytest --benchmark-disable --showlocals
cd -
