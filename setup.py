import os

from setuptools import setup, find_packages


location = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(location, "README.md"), "r") as readme_file:
    long_description = readme_file.read()

setup(
    name="private",
    description="Python modules and distribution encryption tool",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/OmerSarig/private",
    author="Omer Sarig",
    author_email="omer@sarig.co.il",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "License :: Freeware",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 2 :: Only",
        "Topic :: Security :: Cryptography",
        "Topic :: Software Development :: Build Tools",
    ],
    keywords="encrypt package wheel obfuscate",
    packages=find_packages(),
    python_requires=">=2.7",
    install_requires=["pyaes", "click", "wheelmerge", "pip"],
    package_data={"private": ["*.txt"]},
    include_package_data=True,
    entry_points={
        "console_scripts": [
            "private = private:main"
        ]
    }
)
